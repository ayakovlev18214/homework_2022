package ru.tfs.spring.data.service;

import ru.tfs.spring.data.dto.RegionDto;
import ru.tfs.spring.data.model.Region;

public interface RegionService {
    Region createRegion(RegionDto regionDto);
    Region getRegion(String region);
}
