package ru.tfs.spring.data.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import ru.tfs.spring.data.model.IdentityDocument;
import ru.tfs.spring.data.model.IdentityDocumentType;

public interface DocumentRepository extends CrudRepository<IdentityDocument, Long> {
    Optional<IdentityDocument> findByDocumentNumberAndDocumentType(
            String documentNumber,
            IdentityDocumentType documentTyp
    );
}
