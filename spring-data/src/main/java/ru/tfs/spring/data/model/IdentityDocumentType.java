package ru.tfs.spring.data.model;

public enum IdentityDocumentType {
    PASSPORT,
    SNILS,
    INSURANCE_POLICY,
}
