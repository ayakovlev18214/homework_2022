package ru.tfs.spring.data.service;

import java.util.List;

import org.springframework.lang.Nullable;
import ru.tfs.spring.data.dto.PersonDto;
import ru.tfs.spring.data.dto.PersonInfoDto;

public interface PersonService {
    void createPerson(PersonDto personDto);

    void changePerson(long id, PersonDto updatePersonDto, boolean hide);

    PersonDto getPerson(long id);
    PersonInfoDto getPerson(String passport);

    List<PersonInfoDto> getPersons(int page, int size, @Nullable String region);

    boolean checkPersonExists(String name, String passportNumber);
}
