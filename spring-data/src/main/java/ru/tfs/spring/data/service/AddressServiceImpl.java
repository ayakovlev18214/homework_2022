package ru.tfs.spring.data.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tfs.spring.data.dto.AddressDto;
import ru.tfs.spring.data.model.Address;
import ru.tfs.spring.data.repository.AddressRepository;

@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {
    private final AddressRepository repository;
    private final RegionService regionService;

    @Override
    public Address createAddress(AddressDto addressDto) {
        return repository.findByAddress(addressDto.getAddress()).orElse(
                Address.builder()
                        .address(addressDto.getAddress())
                        .region(regionService.createRegion(addressDto.getRegion()))
                        .build()
        );
    }
}
