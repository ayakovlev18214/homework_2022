package ru.tfs.spring.data.repository;

import java.util.Optional;
import java.util.Set;

import javax.validation.constraints.NotBlank;

import lombok.NonNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import ru.tfs.spring.data.dto.PersonInfoDto;
import ru.tfs.spring.data.model.IdentityDocument;
import ru.tfs.spring.data.model.Person;
import ru.tfs.spring.data.model.Region;

public interface PersonRepository extends PagingAndSortingRepository<Person, Long> {
    @EntityGraph(value = "person-entity-graph")
    Optional<Person> findFirstByNameAndDocumentsContains(@NotBlank String name, IdentityDocument document);
    @EntityGraph(value = "person-entity-graph")
    Optional<Person> findByIdAndHidedFalse(Long aLong);
    @EntityGraph(value = "person-entity-graph")
    Optional<Person> findByDocumentsContains(IdentityDocument document);
    @EntityGraph(value = "person-entity-graph")
    Page<Person> findAllByHidedFalse(Pageable pageable);
    @EntityGraph(value = "person-entity-graph")
    Page<Person> findAllByHidedFalseAndRegistrationAddress_Region(Pageable pageable, Region region);
}
