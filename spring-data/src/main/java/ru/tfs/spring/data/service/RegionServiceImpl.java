package ru.tfs.spring.data.service;

import javax.persistence.EntityNotFoundException;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import ru.tfs.spring.data.dto.RegionDto;
import ru.tfs.spring.data.model.Region;
import ru.tfs.spring.data.repository.RegionRepository;

@Service
@RequiredArgsConstructor
public class RegionServiceImpl implements RegionService {
    private final RegionRepository repository;

    @Override
    public Region getRegion(String region) {
        return repository.findByRegion(region).orElseThrow(() -> {
            throw new EntityNotFoundException("Region doesn't exists");
        });
    }

    @Override
    public Region createRegion(RegionDto regionDto) {
        return repository.findByRegion(regionDto.getRegion()).orElse(Region.builder()
                .region(regionDto.getRegion())
                .build()
        );


    }
}
