package ru.tfs.spring.data.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tfs.spring.data.dto.DocumentDto;
import ru.tfs.spring.data.model.IdentityDocument;
import ru.tfs.spring.data.model.IdentityDocumentType;
import ru.tfs.spring.data.model.Person;
import ru.tfs.spring.data.repository.DocumentRepository;

@Service
@RequiredArgsConstructor
public class DocumentServiceImpl implements DocumentService {
    private final DocumentRepository repository;

    @Override
    public IdentityDocument createDocument(DocumentDto documentDto, Person person) {
        return IdentityDocument.builder()
                .documentType(documentDto.getDocumentType())
                .documentNumber(documentDto.getDocumentNumber())
                .main(documentDto.isMainDocument())
                .person(person)
                .build();
    }

    @Override
    public List<IdentityDocument> createDocuments(List<DocumentDto> documentDtos, Person documentOwner) {
        return documentDtos.stream()
                .map(documentDto -> createDocument(documentDto, documentOwner))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<IdentityDocument> findDocument(String number, IdentityDocumentType type) {
        return repository.findByDocumentNumberAndDocumentType(number, type);
    }
}
