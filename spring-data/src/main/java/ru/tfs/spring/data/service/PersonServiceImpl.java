package ru.tfs.spring.data.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tfs.spring.data.dto.AddressDto;
import ru.tfs.spring.data.dto.ContactDto;
import ru.tfs.spring.data.dto.DocumentDto;
import ru.tfs.spring.data.dto.PersonDto;
import ru.tfs.spring.data.dto.PersonInfoDto;
import ru.tfs.spring.data.dto.RegionDto;
import ru.tfs.spring.data.model.Address;
import ru.tfs.spring.data.model.Contact;
import ru.tfs.spring.data.model.ContactType;
import ru.tfs.spring.data.model.IdentityDocument;
import ru.tfs.spring.data.model.IdentityDocumentType;
import ru.tfs.spring.data.model.Person;
import ru.tfs.spring.data.repository.PersonRepository;

@Service
@RequiredArgsConstructor
public class PersonServiceImpl implements PersonService {
    private final PersonRepository personRepository;
    private final AddressService addressService;
    private final ContactService contactService;
    private final DocumentService documentService;
    private final RegionService regionService;

    @Override
    @Transactional(readOnly = true)
    public List<PersonInfoDto> getPersons(int page, int size, @Nullable String region) {
        return (region == null
                ? personRepository.findAllByHidedFalse(PageRequest.of(page, size))
                : personRepository.findAllByHidedFalseAndRegistrationAddress_Region(
                PageRequest.of(page, size),
                regionService.getRegion(region)
        )).map(this::convertPersonToInfo).toList();
    }

    @Override
    public PersonInfoDto getPerson(String passport) {
        return documentService.findDocument(passport, IdentityDocumentType.PASSPORT)
                .map(personRepository::findByDocumentsContains)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(this::convertPersonToInfo).orElseThrow(() -> {
                    throw new IllegalArgumentException("Person doesn't exists");
                });
    }

    @Override
    @Transactional(readOnly = true)
    public PersonDto getPerson(long id) {
        Person person = personRepository.findByIdAndHidedFalse(id).orElseThrow();
        return PersonDto.builder()
                .name(person.getName())
                .birthDate(person.getBirthDate())
                .addresses(person.getAddresses().stream()
                        .map(address -> AddressDto.builder()
                                .address(address.getAddress())
                                .region(new RegionDto(address.getRegion().getRegion()))
                                .registration(address.equals(person.getRegistrationAddress()))
                                .build()
                        ).collect(Collectors.toList())
                ).contacts(person.getContacts().stream()
                        .map(contact -> ContactDto.builder()
                                .contactData(contact.getContact())
                                .contactType(contact.getContactType())
                                .build()
                        ).collect(Collectors.toList())
                ).documents(person.getDocuments().stream()
                        .map(document -> DocumentDto.builder()
                                .documentNumber(document.getDocumentNumber())
                                .mainDocument(document.isMain())
                                .documentType(document.getDocumentType())
                                .build()
                        ).collect(Collectors.toList())
                )
                .build();
    }

    @Override
    @Transactional
    public void changePerson(long id, PersonDto personDto, boolean hide) {
        Person person = personRepository.findById(id).orElseThrow();
        person.setName(personDto.getName());
        person.setBirthDate(personDto.getBirthDate());
        person.setHided(hide);
        person.setRegistrationAddress(null);

        Set.copyOf(person.getAddresses()).forEach(address -> {
            person.removeAddress(address);
            address.removePerson(person);
        });
        Set.copyOf(person.getContacts()).forEach(person::removeContact);
        Set.copyOf(person.getDocuments()).forEach(person::removeDocument);

        createAddresses(person, personDto);
        documentService.createDocuments(personDto.getDocuments(), person).forEach(person::addDocument);
        contactService.createContacts(personDto.getContacts(), person).forEach(person::addContact);
        personRepository.save(person);
    }

    @Override
    @Transactional
    public void createPerson(PersonDto personDto) {
        Person person = Person.builder()
                .name(personDto.getName())
                .birthDate(personDto.getBirthDate())
                .build();

        personRepository.save(person);
        createAddresses(person, personDto);
        documentService.createDocuments(personDto.getDocuments(), person).forEach(person::addDocument);
        contactService.createContacts(personDto.getContacts(), person).forEach(person::addContact);
        personRepository.save(person);
    }

    @Override
    @Transactional(readOnly = true)
    public boolean checkPersonExists(String name, String passportNumber) {
        return documentService.findDocument(passportNumber, IdentityDocumentType.PASSPORT).filter(
                identityDocument -> personRepository.findFirstByNameAndDocumentsContains(name, identityDocument).isPresent()
        ).isPresent();
    }

    private void createAddresses(Person person, PersonDto personDto) {
        validatePersonDto(personDto);

        personDto.getAddresses().forEach(addressDto -> {
            Address address = addressService.createAddress(addressDto);
            address.addPerson(person);
            person.addAddress(address);

            if (addressDto.isRegistration()) {
                person.setRegistrationAddress(address);
            }
        });
    }

    private void validatePersonDto(PersonDto dto) {
        if (dto.getAddresses().stream().filter(AddressDto::isRegistration).count() > 1) {
            throw new IllegalArgumentException("Person can not have two registration addresses");
        }
    }

    private PersonInfoDto convertPersonToInfo(Person person) {
        return PersonInfoDto.builder()
                .id(person.getId())
                .name(person.getName())
                .telephoneNumber(person.getContacts().stream()
                        .filter(contact -> contact.getContactType() == ContactType.PHONE)
                        .findFirst()
                        .map(Contact::getContact)
                        .orElse("")
                ).birthDate(person.getBirthDate())
                .registrationAddress(person.getRegistrationAddress().getAddress())
                .region(person.getRegistrationAddress().getRegion().getRegion())
                .documentNumber(person.getDocuments().stream()
                        .filter(IdentityDocument::isMain).findFirst()
                        .map(IdentityDocument::getDocumentNumber)
                        .orElse("")
                ).build();
    }
}
