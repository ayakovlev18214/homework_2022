package ru.tfs.spring.data.service;

import java.util.List;

import ru.tfs.spring.data.dto.ContactDto;
import ru.tfs.spring.data.model.Contact;
import ru.tfs.spring.data.model.Person;

public interface ContactService {
    Contact createContact(ContactDto contactDto, Person person);
    List<Contact> createContacts(List<ContactDto> contactDtos, Person person);
}
