package ru.tfs.spring.data.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.tfs.spring.data.model.ContactType;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ContactDto {
    private String contactData;
    private ContactType contactType;
}
