package ru.tfs.spring.data.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.tfs.spring.data.model.IdentityDocumentType;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DocumentDto {
    private IdentityDocumentType documentType;
    private String documentNumber;
    private boolean mainDocument;
}
