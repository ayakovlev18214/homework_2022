package ru.tfs.spring.data.dto;

import java.time.LocalDate;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class PersonInfoDto {
    private long id;
    private String name;
    private LocalDate birthDate;
    private String telephoneNumber;
    private String documentNumber;
    private String region;
    private String registrationAddress;
}
