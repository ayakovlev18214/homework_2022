package ru.tfs.spring.data.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import ru.tfs.spring.data.model.Address;

public interface AddressRepository extends CrudRepository<Address, Long> {
    Optional<Address> findByAddress(String address);
}
