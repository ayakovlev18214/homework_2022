package ru.tfs.spring.data.controller;

import java.util.List;

import javax.validation.Valid;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.tfs.spring.data.dto.PersonDto;
import ru.tfs.spring.data.dto.PersonInfoDto;
import ru.tfs.spring.data.service.PersonService;

@RestController
@RequiredArgsConstructor
public class PersonController {
    private final PersonService personService;

    @PostMapping("/person")
    public void createPerson(@Valid @RequestBody PersonDto personDto) {
        personService.createPerson(personDto);
    }

    @PutMapping("/person")
    public void changePerson(
            long personId,
            @Valid @RequestBody PersonDto personDto,
            @RequestParam(required = false, defaultValue = "false") boolean hide
    ) {
        personService.changePerson(personId, personDto, hide);
    }

    @GetMapping("/person/{id}")
    public PersonDto findPerson(@PathVariable long id) {
        return personService.getPerson(id);
    }
    @GetMapping("/person/passport/{passport}")
    public PersonInfoDto findPerson(@PathVariable String passport) {
        return personService.getPerson(passport);
    }

    @GetMapping(path = "/person")
    public List<PersonInfoDto> getPerson(
            @RequestParam("page") int page,
            @RequestParam("size") int size,
            @RequestParam(required = false) String region
    ) {
        return personService.getPersons(page, size, region);
    }

    @GetMapping("/person/verify")
    public boolean verifyPersonExists(
            @RequestParam String name,
            @RequestParam String passport
    ) {
        return personService.checkPersonExists(name, passport);
    }
}
