package ru.tfs.spring.data.service;

import java.util.List;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tfs.spring.data.dto.ContactDto;
import ru.tfs.spring.data.model.Contact;
import ru.tfs.spring.data.model.Person;

@Service
@RequiredArgsConstructor
public class ContactServiceImpl implements ContactService {
    @Override
    public Contact createContact(ContactDto contactDto, Person person) {
        return Contact.builder()
                .contact(contactDto.getContactData())
                .contactType(contactDto.getContactType())
                .person(person)
                .build();
    }

    @Override
    public List<Contact> createContacts(List<ContactDto> contactDtos, Person person) {
        return contactDtos.stream().map(contactDto -> createContact(contactDto, person)).collect(Collectors.toList());
    }
}
