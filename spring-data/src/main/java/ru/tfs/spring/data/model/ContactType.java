package ru.tfs.spring.data.model;

public enum ContactType {
    PHONE,
    EMAIL
}
