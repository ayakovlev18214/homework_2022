package ru.tfs.spring.data.dto;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

@Getter
@AllArgsConstructor
@Builder
public class PersonDto {
    private String name;
    @NonNull
    private LocalDate birthDate;
    @NonNull
    private List<AddressDto> addresses;
    @NonNull
    private List<DocumentDto> documents;
    @NonNull
    private List<ContactDto> contacts;

    public PersonDto() {
        addresses = new ArrayList<>();
        documents = new ArrayList<>();
        contacts = new ArrayList<>();
    }
}
