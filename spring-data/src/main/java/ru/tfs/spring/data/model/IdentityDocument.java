package ru.tfs.spring.data.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "document")
@Getter
@Setter
public class IdentityDocument {
    @Id
    @GeneratedValue
    private Long id;
    private String documentNumber;
    private boolean main;
    @Enumerated(value = EnumType.STRING)
    private IdentityDocumentType documentType;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        IdentityDocument that = (IdentityDocument) o;
        return id.equals(that.id) && documentNumber.equals(that.documentNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, documentNumber);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    private Person person;
}
