package ru.tfs.spring.data.service;

import java.util.List;
import java.util.Optional;

import ru.tfs.spring.data.dto.DocumentDto;
import ru.tfs.spring.data.model.IdentityDocument;
import ru.tfs.spring.data.model.IdentityDocumentType;
import ru.tfs.spring.data.model.Person;

public interface DocumentService {
    IdentityDocument createDocument(DocumentDto documentDto, Person person);

    List<IdentityDocument> createDocuments(List<DocumentDto> documentDtos, Person documentOwner);

    Optional<IdentityDocument> findDocument(String documentNumber, IdentityDocumentType type);
}
