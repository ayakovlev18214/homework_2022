package ru.tfs.spring.data.model;

import java.sql.Date;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@NamedEntityGraph(
        name = "person-entity-graph",
        attributeNodes = {
                @NamedAttributeNode("registrationAddress"),
                @NamedAttributeNode("contacts"),
                @NamedAttributeNode("documents"),
                @NamedAttributeNode(value = "addresses", subgraph = "address-entity-graph"),
        },
        subgraphs = {
                @NamedSubgraph(
                        name = "address-entity-graph",
                        attributeNodes = {
                                @NamedAttributeNode("region"),
                        }
                )
        }
)
public class Person {
    @Id
    @GeneratedValue
    private Long id;
    private LocalDate birthDate;
    @NotBlank
    private String name;
    private boolean hided;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(columnDefinition = "address_id")
    private Address registrationAddress;
    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL, orphanRemoval = true)
    @Builder.Default
    private Set<Contact> contacts = new HashSet<>();

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL, orphanRemoval = true)
    @Builder.Default
    private Set<IdentityDocument> documents = new HashSet<>();

    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "people")
    @Builder.Default
    private Set<Address> addresses = new HashSet<>();

    public void addAddress(Address address) {
        if (!addresses.contains(address)) {
            this.addresses.add(address);
        }
    }

    public void removeContact(Contact contact) {
        contacts.remove(contact);
        contact.setPerson(null);
    }

    public void removeAddress(Address address) {
        addresses.remove(address);
    }

    public void removeDocument(IdentityDocument document) {
        documents.remove(document);
        document.setPerson(null);
    }

    public void addContact(Contact contact) {
        this.contacts.add(contact);
    }

    public void addDocument(IdentityDocument document) {
        this.documents.add(document);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }
        Person person = (Person) o;
        return id != null && Objects.equals(id, person.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, birthDate, hided);
    }
}
