package ru.tfs.spring.data.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import ru.tfs.spring.data.model.Region;

public interface RegionRepository extends CrudRepository<Region, Long> {
    Optional<Region> findByRegion(String region);
}
