package ru.tfs.spring.data.service;

import ru.tfs.spring.data.dto.AddressDto;
import ru.tfs.spring.data.model.Address;

public interface AddressService {
    Address createAddress(AddressDto addressDto);
}
