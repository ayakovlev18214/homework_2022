package ru.tfs.collections_generics.task3;

import java.util.ArrayList;
import java.util.List;

public class Client {
    private long id;
    private String name;
    private int age;
    private List<Phone> phones;

    public void addPhone(Phone phone) {
        phones.add(phone);
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public Client(long id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.phones = new ArrayList<>();
    }
}
