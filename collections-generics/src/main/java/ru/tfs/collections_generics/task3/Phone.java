package ru.tfs.collections_generics.task3;

public class Phone {
    private final String phoneNumber;
    private final PhoneType type;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public PhoneType getType() {
        return type;
    }

    public Phone(String phoneNumber, PhoneType type) {
        this.phoneNumber = phoneNumber;
        this.type = type;
    }
}
