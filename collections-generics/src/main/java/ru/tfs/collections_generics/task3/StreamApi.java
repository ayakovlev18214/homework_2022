package ru.tfs.collections_generics.task3;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class StreamApi {
    /**
     * Get the summary age for the given name from clients array.
     *
     * @param clients clients array to get the age from.
     * @param name    name to count the summary age.
     * @return the summary age.
     */
    public long summaryAgeForName(Client[] clients, String name) {
        return Arrays.stream(clients)
                .filter(client -> client.getName().equals(name))
                .mapToInt(Client::getAge)
                .sum();
    }

    /**
     * Returns names of clients in order they are present in the array.
     *
     * @return set of names
     */
    public Set<String> getNames(Client[] clients) {
        return Arrays.stream(clients)
                .map(Client::getName)
                .collect(Collectors.toSet());
    }

    /**
     * Check if clients array contains client older that given age.
     *
     * @param clients given array
     * @param age     necessary age
     * @return true -> if clients has at least 1 client older than age. false -> otherwise.
     */
    public boolean containsOlderThan(Client[] clients, int age) {
        return Arrays.stream(clients).anyMatch(client -> client.getAge() > age);
    }

    /**
     * Converts array to Map of client id and name.
     *
     * @param clients given array to transform.
     * @return the map, where key is the unique client id and the value is client name.
     */
    public Map<Long, String> getClientIdToName(Client[] clients) {
        return Arrays.stream(clients).collect(Collectors.toMap(Client::getId, Client::getName));
    }

    /**
     * Converts array to Map of age to clients list.
     *
     * @param clients given array to transform.
     * @return the map, where key is age and the value is list of clients with this age.
     */
    public Map<Integer, List<Client>> getAgeToClients(Client[] clients) {
        return Arrays.stream(clients).collect(Collectors.groupingBy(Client::getAge));
    }

    /**
     * Get all phones from clients separated by commas.
     *
     * @param clients array to take phones.
     * @return string, containing all phones from all clients.
     */
    public String getAllPhones(Client[] clients) {
        return Arrays.stream(clients)
                .map(Client::getPhones)
                .flatMap(Collection::stream)
                .map(Phone::getPhoneNumber)
                .collect(Collectors.joining(","));
    }

    /**
     * Get the oldest person with stationary phone.
     *
     * @param clients the clients array.
     * @return Client, who has stationary phone and the most age in the clients array.
     */
    public Client getMostOldClientWithStationaryPhone(Client[] clients) {
        return Arrays.stream(clients)
                .filter(client -> client.getPhones().stream()
                        .map(Phone::getType)
                        .anyMatch(phoneType -> phoneType == PhoneType.STATIONARY)
                ).max(Comparator.comparing(Client::getAge)).orElse(null);
    }

}
