package ru.tfs.collections_generics.task2;

public class Post {
    private final String text;
    private final Integer likesCount;

    public Post(String text, Integer likesCount) {
        this.text = text;
        this.likesCount = likesCount;
    }

    public Integer getLikesCount() {
        return likesCount;
    }
}
