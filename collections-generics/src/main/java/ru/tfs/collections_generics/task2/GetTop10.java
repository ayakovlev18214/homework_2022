package ru.tfs.collections_generics.task2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.PriorityQueue;

public class GetTop10 {
    public static List<Post> getTop10(List<Post> posts) {
        return getTopN(posts, 10);
    }

    private static List<Post> getTopN(List<Post> posts, int N) {
        PriorityQueue<Post> topPosts = new PriorityQueue<>(N, Comparator.comparing(Post::getLikesCount));
        for (Post post : posts) {
            if (topPosts.size() < N) {
                topPosts.offer(post);
            } else if (topPosts.peek() != null && topPosts.peek().getLikesCount() < post.getLikesCount()) {
                topPosts.poll();
                topPosts.offer(post);
            }
        }
        return new ArrayList<>(topPosts);
    }
}
