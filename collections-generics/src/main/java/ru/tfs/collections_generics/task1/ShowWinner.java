package ru.tfs.collections_generics.task1;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShowWinner {
    public static void showWinner(List<String> competitors) {
        Map<String, Integer> competitorToScore = new HashMap<>();
        String maxCompetitor = "";
        int maxScore = -1;
        for (String competitor : competitors) {
            String[] competitorAndScore = competitor.split(" ");
            int newScore = competitorToScore.merge(
                    competitorAndScore[0],
                    Integer.valueOf(competitorAndScore[1]),
                    Integer::sum
            );
            if (newScore > maxScore) {
                maxCompetitor = competitorAndScore[0];
                maxScore = newScore;
            }
        }
        System.out.println(maxCompetitor);
    }
}
