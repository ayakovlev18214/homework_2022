package ru.tfs.collections_generics.task4;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;

public class SearchServiceImpl implements SearchService {
    @Override
    public List<User> searchForFriendsInWidth(User me, String name) {
        List<User> result = new ArrayList<>();
        Set<User> visited = new HashSet<>();
        Queue<User> queue = new ArrayDeque<>(me.getFriends());
        visited.add(me);
        while (!queue.isEmpty()) {
            User user = queue.poll();
            if (visited.contains(user)) {
                continue;
            }
            visited.add(user);
            if (user.getName().equals(name)) {
                result.add(user);
            }
            queue.addAll(user.getFriends());
        }
        return result;
    }

    @Override
    public List<User> searchForFriendsInDepth(User me, String name) {
        List<User> usersToVisit = new ArrayList<>();
        HashSet<User> visitedUsers = new HashSet<>();
        List<User> result = new ArrayList<>();
        usersToVisit.add(me);
        while (!usersToVisit.isEmpty()) {
            User user = usersToVisit.remove(0);
            if (visitedUsers.contains(user)) {
                continue;
            }
            visitedUsers.add(user);
            usersToVisit.addAll(0, user.getFriends());
            if (user.getName().equals(name)) {
                result.add(user);
            }
        }
        return result;
    }
}
