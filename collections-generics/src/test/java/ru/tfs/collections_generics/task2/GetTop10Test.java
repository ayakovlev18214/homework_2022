package ru.tfs.collections_generics.task2;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class GetTop10Test {
    @Test
    void testEmptyCollection() {
        Assertions.assertTrue(GetTop10.getTop10(List.of()).isEmpty());
    }

    @Test
    void testDefaultCase() {
        List<Post> top10Posts = List.of(
                new Post("0", 1010),
                new Post("0", 3040),
                new Post("0", 4050),
                new Post("0", 9993),
                new Post("0", 3000),
                new Post("0", 3333),
                new Post("0", 515135),
                new Post("0", 5353),
                new Post("0", 88336),
                new Post("0", 300340)
        );

        List<Post> posts = new ArrayList<>(List.of(
                new Post("0", 113),
                new Post("0", 232),
                new Post("0", 352),
                new Post("0", 552),
                new Post("0", 63),
                new Post("0", 94),
                new Post("0", 95)
        ));
        posts.addAll(top10Posts);

        Assertions.assertTrue(GetTop10.getTop10(posts).containsAll(top10Posts));
    }
}
