package ru.tfs.collections_generics.task1;

import java.util.List;

import org.junit.jupiter.api.Test;

public class ShowWinnerTest {
    @Test
    void emptyCollectionTest() {
        ShowWinner.showWinner(List.of());
    }

    @Test
    void defaultTest() {
        ShowWinner.showWinner(
                List.of("Ivan 5", "Petr 3", "Alex 10", "Petr 8", "Ivan 6", "Alex 5", "Ivan 1", "Petr 5", "Alex 1")
        );
    }
}
