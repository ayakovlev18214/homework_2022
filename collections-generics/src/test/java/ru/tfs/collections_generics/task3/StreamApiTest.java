package ru.tfs.collections_generics.task3;

import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StreamApiTest {
    private final StreamApi streamApi = new StreamApi();

    @Test
    void summarizedAge() {
        Assertions.assertEquals(30, streamApi.summaryAgeForName(getClients(), "Vasya"));
    }

    @Test
    void getNamesSet() {
        Assertions.assertEquals(Set.of("Vasya", "Petya", "Vova", "Ivan"), streamApi.getNames(getClients()));
    }

    @Test
    void checkHasOlder() {
        Assertions.assertTrue(streamApi.containsOlderThan(getClients(), 69));
        Assertions.assertFalse(streamApi.containsOlderThan(getClients(), 70));
    }

    @Test
    void getMapIdToName() {
        var map = streamApi.getClientIdToName(getClients());
        Assertions.assertEquals("Vova", map.get(3L));
        Assertions.assertEquals("Ivan", map.get(10L));
    }

    @Test
    void getMapAgeToClientList() {
        var map = streamApi.getAgeToClients(getClients());
        Assertions.assertEquals(2, map.get(10).size());
    }

    @Test
    void getPhones() {
        Assertions.assertEquals(
                "8-800-555-3535,8-777-666-555,8-999-999-9999",
                streamApi.getAllPhones(getClients())
        );
    }

    @Test
    void getOldestClientWithStationaryPhone() {
        Client[] clients = getClients();
        Assertions.assertEquals(
                clients[0],
                streamApi.getMostOldClientWithStationaryPhone(clients)
        );
    }

    private Client[] getClients() {
        Client[] clients = new Client[10];

        clients[0] = new Client(1, "Vasya", 10);
        clients[0].addPhone(new Phone("8-800-555-3535", PhoneType.STATIONARY));
        clients[1] = new Client(2, "Petya", 9);
        clients[2] = new Client(3, "Vova", 20);
        clients[3] = new Client(4, "Vasya", 5);
        clients[3].addPhone(new Phone("8-777-666-555", PhoneType.STATIONARY));
        clients[4] = new Client(5, "Petya", 10);
        clients[5] = new Client(6, "Vova", 30);
        clients[6] = new Client(7, "Vasya", 15);
        clients[7] = new Client(8, "Petya", 33);
        clients[8] = new Client(9, "Vova", 40);
        clients[8].addPhone(new Phone("8-999-999-9999", PhoneType.MOBILE));
        clients[9] = new Client(10, "Ivan", 70);

        return clients;
    }
}
