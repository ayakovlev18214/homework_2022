insert into vaccine(id, name)
values (1, 'SPUTNIK'),
       (2, 'VECTOR'),
       (3, '5G');

insert into vaccination_point(id, address, name)
values (1, 'Nikolaeva 11', 'GEMOTEST-1'),
       (2, 'DOM KOLOTUSHKINA', 'PUSHKINA'),
       (3, 'Morskoy street 14', 'GEMOTEST-NSK')
