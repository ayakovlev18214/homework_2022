package ru.tfs.spring.cloud.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@NamedEntityGraph(
        name = "vaccination-entity-graph",
        attributeNodes = {
                @NamedAttributeNode("vaccinationPoint"),
                @NamedAttributeNode("vaccine"),
        }
)
public class Vaccination {
    @Id
    @GeneratedValue
    private Long id;
    private LocalDate vaccinationDate;
    private String patient;
    private String documentNumber;
    private boolean pushed;

    @JoinColumn(referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Vaccine vaccine;

    @JoinColumn(referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaccinationPoint vaccinationPoint;

    @Override
    public String toString() {
        return "Vaccination{" +
                "vaccinationDate=" + vaccinationDate +
                ", patient='" + patient + '\'' +
                ", documentNumber='" + documentNumber + '\'' +
                ", vaccine=" + vaccine +
                ", vaccinationPoint=" + vaccinationPoint +
                '}';
    }
}
