package ru.tfs.spring.cloud.service;

import java.io.IOException;
import java.text.ParseException;

import com.opencsv.exceptions.CsvValidationException;
import org.springframework.web.multipart.MultipartFile;

public interface RegistryService {
    void store(MultipartFile file) throws IOException, CsvValidationException, ParseException;
}
