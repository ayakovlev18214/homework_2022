package ru.tfs.spring.cloud.service;

import java.time.LocalDate;
import java.util.Optional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.tfs.spring.cloud.dto.VaccinationDto;
import ru.tfs.spring.cloud.model.Vaccination;
import ru.tfs.spring.cloud.model.VaccinationPoint;
import ru.tfs.spring.cloud.model.Vaccine;
import ru.tfs.spring.cloud.repository.VaccinationRepository;

@Service
@RequiredArgsConstructor
@Slf4j
public class VaccinationServiceImpl implements VaccinationService {
    private final VaccinationRepository repository;

    @Override
    public VaccinationDto getVaccination(String document) {
        Vaccination vaccination = repository.findByDocumentNumberOrderByVaccinationDateDesc(document);

        return VaccinationDto.builder()
                .vaccinationPointName(vaccination.getVaccinationPoint().getName())
                .vaccinationPointAddress(vaccination.getVaccinationPoint().getAddress())
                .patient(vaccination.getPatient())
                .documentNumber(vaccination.getDocumentNumber())
                .vaccinationDate(vaccination.getVaccinationDate())
                .vaccineName(vaccination.getVaccine().getName())
                .build();
    }

    @Override
    public Vaccination getVaccination(
            LocalDate vaccinationDate,
            String patient,
            String documentNumber,
            Vaccine vaccine,
            VaccinationPoint vaccinationPoint) {
        Optional<Vaccination> vaccination = repository.findFirstByDocumentNumberAndVaccinationDate(
                documentNumber,
                vaccinationDate
        );

        if (vaccination.isPresent()) {
            log.info(String.format(
                    "Vaccination with date = [%s] and document = [%s] already exists",
                    vaccinationDate.toString(),
                    documentNumber
            ));
        }

        return vaccination.orElse(repository.save(
                Vaccination.builder()
                        .vaccinationDate(vaccinationDate)
                        .patient(patient)
                        .documentNumber(documentNumber)
                        .pushed(false)
                        .vaccine(vaccine)
                        .vaccinationPoint(vaccinationPoint)
                        .build())
        );
    }
}
