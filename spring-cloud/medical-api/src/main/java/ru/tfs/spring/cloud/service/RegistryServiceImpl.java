package ru.tfs.spring.cloud.service;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.multipart.MultipartFile;
import ru.tfs.spring.cloud.dto.QrCodeInfo;
import ru.tfs.spring.cloud.client.PersonClient;
import ru.tfs.spring.cloud.model.Vaccination;
import ru.tfs.spring.cloud.model.VaccinationPoint;
import ru.tfs.spring.cloud.model.Vaccine;
import ru.tfs.spring.cloud.repository.VaccinationRepository;

@Service
@RequiredArgsConstructor
@Slf4j
public class RegistryServiceImpl implements RegistryService {
    private static final String KAFKA_TOPIC = "qr.codes";
    private final VaccinationService vaccinationService;
    private final VaccineService vaccineService;
    private final VaccinationPointService vaccinationPointService;
    private final VaccinationRepository repository;
    private final PersonClient personClient;
    private final KafkaTemplate<String, QrCodeInfo> kafkaProducer;

    @Override
    public void store(MultipartFile file) throws IOException, CsvValidationException, ParseException {
        try (Reader reader = new InputStreamReader(file.getInputStream());
             CSVReader csvReader = new CSVReader(reader)
        ) {
            String[] line;
            while ((line = csvReader.readNext()) != null) {
                String personName = line[0];
                String documentNum = line[1];
                Vaccine vaccine = vaccineService.getVaccine(line[3]);
                VaccinationPoint vaccinationPoint = vaccinationPointService.getVaccinationPoint(
                        Long.valueOf(line[4]),
                        line[5],
                        line[6]
                );
                if (vaccine == null) {
                    log.warn(String.format("Vaccine [%s] doesn't exists for person [%s, %s, %s]",
                            line[3],
                            personName, documentNum, line[2]
                    ));
                    continue;
                }
                if (vaccinationPoint == null) {
                    log.warn(String.format("Vaccination point [%s, %s, %s] doesn't exists for person [%s, %s, %s]",
                            line[4], line[5], line[6],
                            personName, documentNum, line[2]
                    ));
                    continue;
                }
                if (personClient.verifyPersonExists(personName, documentNum)) {
                    Vaccination vaccination = vaccinationService.getVaccination(
                            LocalDate.parse(line[2], DateTimeFormatter.ISO_DATE),
                            personName,
                            documentNum,
                            vaccine,
                            vaccinationPoint
                    );
                    saveVaccinationInfo(vaccination);
                } else {
                    log.warn(String.format("Person [%s, %s, %s] doesn't exists in PersonService",
                            personName,
                            documentNum,
                            line[2]
                    ));
                }
            }
        }
    }

    @Transactional
    void saveVaccinationInfo(Vaccination vaccination) {
        repository.save(vaccination);
    }

    @Scheduled(fixedDelay = 1000)
    public void scheduleFixedDelayTask() {
        repository.findAllByPushedFalse().forEach(vaccination -> {
            QrCodeInfo qrCodeInfo = QrCodeInfo.builder()
                    .personDocumentNumber(vaccination.getDocumentNumber())
                    .personName(vaccination.getPatient())
                    .vaccinationDate(vaccination.getVaccinationDate())
                    .build();
            var record = new ProducerRecord<>(KAFKA_TOPIC, "", qrCodeInfo);
            var future = kafkaProducer.send(record);
            future.addCallback(new ListenableFutureCallback<>() {
                @Override
                public void onFailure(Throwable ex) {
                    log.error("Failed to send " + qrCodeInfo.toString(), ex);
                }

                @Override
                public void onSuccess(SendResult<String, QrCodeInfo> result) {
                    vaccination.setPushed(true);
                    saveVaccinationInfo(vaccination);
                }
            });
        });
    }
}
