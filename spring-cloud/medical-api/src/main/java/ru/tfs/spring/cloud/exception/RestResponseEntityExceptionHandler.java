package ru.tfs.spring.cloud.exception;

import java.io.IOException;
import java.text.ParseException;
import java.util.NoSuchElementException;

import javax.persistence.EntityNotFoundException;

import com.opencsv.exceptions.CsvValidationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = {CsvValidationException.class, IOException.class, ParseException.class})
    protected ResponseEntity<Object> handelNotFound(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, "Can't read csv file.", new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
}
