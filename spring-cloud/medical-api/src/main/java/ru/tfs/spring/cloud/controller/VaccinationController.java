package ru.tfs.spring.cloud.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.tfs.spring.cloud.dto.VaccinationDto;
import ru.tfs.spring.cloud.model.Vaccination;
import ru.tfs.spring.cloud.service.VaccinationService;

@RestController
@RequiredArgsConstructor
public class VaccinationController {
    private final VaccinationService vaccinationService;

    @GetMapping(path = "/vaccination")
    public VaccinationDto getVaccinationInfo(@RequestParam String document) {
        return vaccinationService.getVaccination(document);
    }
}
