package ru.tfs.spring.cloud.service;

import java.time.LocalDate;

import ru.tfs.spring.cloud.dto.VaccinationDto;
import ru.tfs.spring.cloud.model.Vaccination;
import ru.tfs.spring.cloud.model.VaccinationPoint;
import ru.tfs.spring.cloud.model.Vaccine;

public interface VaccinationService {
    Vaccination getVaccination(
            LocalDate vaccinationDate,
            String patient,
            String documentNumber,
            Vaccine vaccine,
            VaccinationPoint vaccinationPoint
    );

    VaccinationDto getVaccination(String document);
}
