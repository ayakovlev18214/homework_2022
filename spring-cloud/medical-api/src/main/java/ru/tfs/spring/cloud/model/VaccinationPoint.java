package ru.tfs.spring.cloud.model;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class VaccinationPoint {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String address;

    @Override
    public String toString() {
        return "VaccinationPoint{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
