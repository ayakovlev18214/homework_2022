package ru.tfs.spring.cloud.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "person", fallback = PersonClient.PersonFallback.class)
public interface PersonClient {
    @GetMapping("/person/verify")
    boolean verifyPersonExists(
            @RequestParam("name") String name,
            @RequestParam("passport") String passport
    );
    class PersonFallback implements PersonClient {
        @Override
        public boolean verifyPersonExists(String name, String passport) {
            return false;
        }
    }
}
