package ru.tfs.spring.cloud.dto;

import java.time.LocalDate;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class QrCodeInfo {
    private String personName;
    private String personDocumentNumber;
    private LocalDate vaccinationDate;
}
