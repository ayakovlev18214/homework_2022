package ru.tfs.spring.cloud.service;

import ru.tfs.spring.cloud.model.Vaccine;

public interface VaccineService {
    Vaccine getVaccine(String vaccineName);
}
