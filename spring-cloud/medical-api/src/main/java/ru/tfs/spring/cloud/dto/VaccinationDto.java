package ru.tfs.spring.cloud.dto;

import java.time.LocalDate;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class VaccinationDto {
    private LocalDate vaccinationDate;
    private String patient;
    private String documentNumber;
    private String vaccineName;
    private String vaccinationPointName;
    private String vaccinationPointAddress;
}
