package ru.tfs.spring.cloud.repository;

import org.springframework.data.repository.CrudRepository;
import ru.tfs.spring.cloud.model.VaccinationPoint;

public interface VaccinationPointRepository extends CrudRepository<VaccinationPoint, Long> {
}
