package ru.tfs.spring.cloud.service;

import java.util.Optional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.tfs.spring.cloud.model.VaccinationPoint;
import ru.tfs.spring.cloud.repository.VaccinationPointRepository;

@Service
@RequiredArgsConstructor
@Slf4j
public class VaccinationPointServiceImpl implements VaccinationPointService {
    private final VaccinationPointRepository repository;

    @Override
    public VaccinationPoint getVaccinationPoint(Long id, String name, String address) {
        Optional<VaccinationPoint> vaccinationPoint = repository.findById(id);
        if (vaccinationPoint.isEmpty()) {
            log.warn(String.format("Can't get vaccination point with [%d, %s, %s]", id, name, address));
        }
        return vaccinationPoint.orElse(null);
    }
}
