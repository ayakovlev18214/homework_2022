package ru.tfs.spring.cloud.service;

import java.util.Optional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.tfs.spring.cloud.model.Vaccine;
import ru.tfs.spring.cloud.repository.VaccineRepository;

@RequiredArgsConstructor
@Service
@Slf4j
public class VaccineServiceImpl implements VaccineService {
    private final VaccineRepository repository;

    @Override
    public Vaccine getVaccine(String vaccineName) {
        Optional<Vaccine> vaccine = repository.findByName(vaccineName);
        if (vaccine.isEmpty()) {
            log.warn(String.format("Can't get vaccine with name [%s]", vaccineName));
        }
        return vaccine.orElse(null);
    }
}
