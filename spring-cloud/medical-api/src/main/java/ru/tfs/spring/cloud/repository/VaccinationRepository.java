package ru.tfs.spring.cloud.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import ru.tfs.spring.cloud.model.Vaccination;

public interface VaccinationRepository extends CrudRepository<Vaccination, Long> {
    Optional<Vaccination> findFirstByDocumentNumberAndVaccinationDate(String documentNumber, LocalDate vaccinationDate);
    List<Vaccination> findAllByPushedFalse();
    @EntityGraph("vaccination-entity-graph")
    Vaccination findByDocumentNumberOrderByVaccinationDateDesc(String documentNumber);
}
