package ru.tfs.spring.cloud.service;

import ru.tfs.spring.cloud.model.VaccinationPoint;

public interface VaccinationPointService {
    VaccinationPoint getVaccinationPoint(Long id, String name, String address);
}
