package ru.tfs.spring.cloud.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import ru.tfs.spring.cloud.model.Vaccine;

public interface VaccineRepository extends CrudRepository<Vaccine, Long> {
    Optional<Vaccine> findByName(String name);
}
