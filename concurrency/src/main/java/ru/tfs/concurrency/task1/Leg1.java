package ru.tfs.concurrency.task1;

import java.util.concurrent.Semaphore;

public class Leg1 implements Runnable {

    private final String name;
    private final Semaphore semaphore;
    private final Semaphore nextSemaphore;

    public Leg1(String name, Semaphore semaphore, Semaphore nextSemaphore) {
        this.name = name;
        this.semaphore = semaphore;
        this.nextSemaphore = nextSemaphore;
    }

    public static void main(String[] args) {
        Semaphore s1 = new Semaphore(0);
        Semaphore s2 = new Semaphore(0);
        new Thread(new Leg1("left", s1, s2)).start();
        new Thread(new Leg1("right", s2, s1)).start();
        s1.release();
    }

    @Override
    public void run() {
        while (true) {
            try {
                semaphore.acquire();
                System.out.println(name);
                nextSemaphore.release();
            } catch (InterruptedException ignored) {
            }
        }
    }
}
