package ru.tfs.concurrency.task1;

import java.util.concurrent.CompletableFuture;

public class Leg2 implements Runnable {

    private final String name;

    public Leg2(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        while (true) {
            CompletableFuture.runAsync(new Leg2("first")).thenRunAsync(new Leg2("second")).join();
        }
    }

    @Override
    public void run() {
        System.out.println(name);
    }
}
