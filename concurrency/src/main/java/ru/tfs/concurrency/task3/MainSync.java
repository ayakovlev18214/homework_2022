package ru.tfs.concurrency.task3;

import java.util.concurrent.Semaphore;

public class MainSync {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(5);
        for (int i = 1; i <= 8; i++) {
            int finalI = i;
            Thread thread = new Thread(() -> {
                try {
                    semaphore.acquire();
                    workOnMachine(finalI);
                    semaphore.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            thread.start();
        }
    }

    private static void workOnMachine(int workerId) {
        try {
            System.out.println("worker " + workerId + " occupy production machine ...");
            Thread.sleep(2000);
            System.out.println("worker " + workerId + " release production machine");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
