package ru.tfs.concurrency.task3;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainThreadPool {

    public static void main(String[] args) {
        ExecutorService threadPoolExecutor = Executors.newFixedThreadPool(5);
        for(int i = 1; i <= 8; i++) {
            int finalI = i;
            threadPoolExecutor.execute(() -> workOnMachine(finalI));
        }
        threadPoolExecutor.shutdown();
    }

    private static void workOnMachine(int workerId) {
        try {
            System.out.println("worker " + workerId + " occupy production machine ...");
            Thread.sleep(2000);
            System.out.println("worker " + workerId + " release production machine");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
