package ru.tfs.concurrency.task2;

import java.util.concurrent.CompletableFuture;

class AccountMainDeadlock {

    public static void main(String[] args) {
        Account firstAccount = new Account(100_000);
        Account secondAccount = new Account(100_000);

        AccountThreadDeadlock firstThread = new AccountThreadDeadlock(firstAccount, secondAccount, 100);
        AccountThreadDeadlock secondThread = new AccountThreadDeadlock(secondAccount, firstAccount, 100);

        CompletableFuture.allOf(
                CompletableFuture.runAsync(firstThread),
                CompletableFuture.runAsync(secondThread)
        ).join();

        System.out.println("Cash balance of the first account: " + firstAccount.getCacheBalance());
        System.out.println("Cash balance of the second account: " + secondAccount.getCacheBalance());
    }
}

class AccountMain {

    public static void main(String[] args) {
        AccountThreadSafe firstAccount = new AccountThreadSafe(100_000, 1L);
        AccountThreadSafe secondAccount = new AccountThreadSafe(100_000, 2L);

        AccountThread firstThread = new AccountThread(firstAccount, secondAccount, 100);
        AccountThread secondThread = new AccountThread(secondAccount, firstAccount, 100);

        CompletableFuture.allOf(
                CompletableFuture.runAsync(firstThread),
                CompletableFuture.runAsync(secondThread)
        ).join();

        System.out.println("Cash balance of the first account: " + firstAccount.getCacheBalance());
        System.out.println("Cash balance of the second account: " + secondAccount.getCacheBalance());
    }
}
