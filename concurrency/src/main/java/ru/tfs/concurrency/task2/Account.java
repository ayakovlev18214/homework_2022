package ru.tfs.concurrency.task2;

import java.util.concurrent.Semaphore;

public class Account {

    private int cacheBalance;
    private final Object lock = new Object();
    private final Semaphore semaphore = new Semaphore(1);

    public Account(int cacheBalance) {
        this.cacheBalance = cacheBalance;
    }

    public void addMoney(int money) {
        try {
            semaphore.acquire();
            this.cacheBalance += money;
            semaphore.release();
        } catch (InterruptedException ignored) {
        }
    }

    public Object getLock() {
        return lock;
    }

    public boolean takeOffMoney(int money) {
        try {
            semaphore.acquire();
            if (this.cacheBalance < money) {
                semaphore.release();
                return false;
            }
            this.cacheBalance -= money;
            semaphore.release();
        } catch (InterruptedException ignored) {
        }
        return true;
    }

    public int getCacheBalance() {
        return cacheBalance;
    }

}
