package ru.tfs.concurrency.task2;

public class AccountThreadSafe {

    private int cacheBalance;
    private final long id;

    public long getId() {
        return id;
    }

    public AccountThreadSafe(int cacheBalance, long id) {
        this.cacheBalance = cacheBalance;
        this.id = id;
    }

    public void addMoney(int money) {
        cacheBalance += money;
    }

    public boolean takeOffMoney(int money) {
        if (cacheBalance < money) {
            return false;
        }
        cacheBalance -= money;
        return true;
    }

    public int getCacheBalance() {
        return cacheBalance;
    }

}
