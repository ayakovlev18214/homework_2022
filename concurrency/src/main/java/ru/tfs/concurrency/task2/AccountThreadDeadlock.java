package ru.tfs.concurrency.task2;

public class AccountThreadDeadlock implements Runnable {

    private final Account accountFrom;
    private final Account accountTo;
    private final int money;

    public AccountThreadDeadlock(Account accountFrom, Account accountTo, int money) {
        this.accountFrom = accountFrom;
        this.accountTo = accountTo;
        this.money = money;
    }

    @Override
    public void run() {
        for (int i = 0; i < 4000; i++) {
            synchronized (accountFrom.getLock()) {
                if (accountFrom.takeOffMoney(money)) {
                    synchronized (accountTo.getLock()) {
                        accountTo.addMoney(money);
                    }
                }
            }
        }
    }
}
