package ru.tfs.concurrency.task2;

public class AccountThread implements Runnable {

    private final AccountThreadSafe accountFrom;
    private final AccountThreadSafe accountTo;
    private final int money;

    public AccountThread(AccountThreadSafe accountFrom, AccountThreadSafe accountTo, int money) {
        this.accountFrom = accountFrom;
        this.accountTo = accountTo;
        this.money = money;
    }

    @Override
    public void run() {
        for (int i = 0; i < 4000; i++) {
            Object lock1 = accountFrom.getId() < accountTo.getId() ? accountFrom : accountTo;
            Object lock2 = accountFrom.getId() < accountTo.getId() ? accountTo : accountFrom;
            synchronized (lock1) {
                synchronized (lock2) {
                    if (accountFrom.takeOffMoney(money)) {
                        accountTo.addMoney(money);
                    }
                }
            }

        }
    }
}
