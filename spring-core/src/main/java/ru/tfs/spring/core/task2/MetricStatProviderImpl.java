package ru.tfs.spring.core.task2;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class MetricStatProviderImpl implements MetricStatProvider {
    private final Map<String, List<MethodCallStat>> methodMetricStats;

    public MetricStatProviderImpl() {
        methodMetricStats = new ConcurrentHashMap<>();
    }

    @Override
    public List<MethodMetricStat> getTotalStatForPeriod(LocalDateTime from, LocalDateTime to) {
        List<MethodMetricStat> list = new ArrayList<>();
        methodMetricStats.forEach((name, methodMetricStats) ->
                getMethodMetricStatForMethod(name, methodMetricStats, from, to).ifPresent(list::add)
        );
        return list;
    }

    @Override
    public MethodMetricStat getTotalStatByMethodForPeriod(String method, LocalDateTime from, LocalDateTime to) {
        return getMethodMetricStatForMethod(method, methodMetricStats.get(method), from, to).orElse(null);
    }

    private Optional<MethodMetricStat> getMethodMetricStatForMethod(
            String methodName,
            List<MethodCallStat> methodCallStats,
            LocalDateTime from,
            LocalDateTime to
    ) {
        if (methodCallStats == null) {
            throw new IllegalArgumentException(String.format("Method [%s] doesn't marked as @Timed", methodName));
        }
        List<MethodCallStat> callStats = methodCallStats.stream()
                .filter(stat -> stat.getStartTime().isAfter(from) && stat.getEndTime().isBefore(to))
                .collect(Collectors.toList());
        if (!callStats.isEmpty()) {
            MethodMetricStat methodMetricStat = new MethodMetricStat(methodName);
            callStats.forEach(call -> computeMethodStats(methodMetricStat, call));
            return Optional.of(methodMetricStat);
        }
        return Optional.empty();
    }

    private void computeMethodStats(MethodMetricStat methodMetricStat, MethodCallStat callStat) {
        methodMetricStat.setInvocationsCount(methodMetricStat.getInvocationsCount() + 1);
        methodMetricStat.setFullWorkTime(methodMetricStat.getFullWorkTime() + callStat.getWorkTime());
        if (callStat.getWorkTime() < methodMetricStat.getMinTime()) {
            methodMetricStat.setMinTime(callStat.getWorkTime());
        }
        if (methodMetricStat.getMaxTime() < callStat.getWorkTime()) {
            methodMetricStat.setMaxTime(callStat.getWorkTime());
        }
        methodMetricStat.setAverageTime(methodMetricStat.getFullWorkTime() / methodMetricStat.getInvocationsCount());
    }

    @Override
    public void addMethodCall(String methodName, MethodCallStat callStat) {
        methodMetricStats.putIfAbsent(methodName, new ArrayList<>());
        methodMetricStats.get(methodName).add(callStat);
    }
}
