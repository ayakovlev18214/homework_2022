package ru.tfs.spring.core.task2;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext("ru.tfs.spring.core.task2");
        UselessInterface uselessInterface = applicationContext.getBean(UselessInterface.class);
        for (int i = 0; i < 100; i++) {
            uselessInterface.doJob(30);
        }
        uselessInterface.doNotTImedJob(50);

        MetricStatProvider provider = applicationContext.getBean(MetricStatProvider.class);

        List<MethodMetricStat> metricStats = provider.getTotalStatForPeriod(
                LocalDateTime.now().minus(Duration.ofMinutes(5L)),
                LocalDateTime.now()
        );
        metricStats.forEach(System.out::println);

        System.out.println(provider.getTotalStatByMethodForPeriod(
                "doJob",
                LocalDateTime.now().minus(Duration.ofMinutes(5L)),
                LocalDateTime.now()
        ));
    }
}


