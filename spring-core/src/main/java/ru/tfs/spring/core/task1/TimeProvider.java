package ru.tfs.spring.core.task1;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class TimeProvider {
    private final SimpleDateFormat simpleDateFormatIso;
    private final SimpleDateFormat simpleDateFormat;

    public TimeProvider(
            @Qualifier("today-iso") SimpleDateFormat simpleDateFormatIso,
            @Qualifier("today") SimpleDateFormat simpleDateFormat
    ) {
        this.simpleDateFormatIso = simpleDateFormatIso;
        this.simpleDateFormat = simpleDateFormat;
    }

    public String getTime(String command) {
        switch (command) {
            case "today":
                return simpleDateFormat.format(System.currentTimeMillis());
            case "today-iso":
                return simpleDateFormatIso.format(System.currentTimeMillis());
            default:
                return String.format("Неизвестная команда [%s].\n", command);
        }
    }
}
