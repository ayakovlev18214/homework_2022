package ru.tfs.spring.core.task2;

public class MethodMetricStat {
    /**
     * Наименование/идентификатор метода
     */
    private final String methodName;

    /**
     * Кол-во вызовов метода
     */
    private long invocationsCount;

    /**
     * Минимальное время работы метода
     */
    private long minTime;

    /**
     * Среднее время работы метода
     */
    private long averageTime;

    /**
     * максимальное время работы метода
     */
    private long maxTime;

    /**
     * Общее время работы метода
     */
    private long fullWorkTime;

    public void setInvocationsCount(long invocationsCount) {
        this.invocationsCount = invocationsCount;
    }

    public void setMinTime(long minTime) {
        this.minTime = minTime;
    }

    public void setAverageTime(long averageTime) {
        this.averageTime = averageTime;
    }

    public void setMaxTime(long maxTime) {
        this.maxTime = maxTime;
    }

    public long getFullWorkTime() {
        return fullWorkTime;
    }

    public void setFullWorkTime(long fullWorkTime) {
        this.fullWorkTime = fullWorkTime;
    }

    public MethodMetricStat(String methodName) {
        this.methodName = methodName;
        this.maxTime = 0L;
        this.minTime = Integer.MAX_VALUE;
        this.averageTime = 0L;
        this.invocationsCount = 0L;
        this.fullWorkTime = 0L;
    }
    public String getMethodName() {
        return methodName;
    }

    public long getInvocationsCount() {
        return invocationsCount;
    }

    public long getMinTime() {
        return minTime;
    }

    public long getAverageTime() {
        return averageTime;
    }

    public long getMaxTime() {
        return maxTime;
    }

    @Override
    public String toString() {
        return "MethodMetricStat{" +
                "methodName='" + methodName + '\'' +
                ", invocationsCount=" + invocationsCount +
                ", minTime=" + minTime +
                ", averageTime=" + averageTime +
                ", maxTime=" + maxTime +
                ", fullWorkTime=" + fullWorkTime +
                '}';
    }
}
