package ru.tfs.spring.core.task1;

import java.text.SimpleDateFormat;
import java.util.Locale;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

@Configuration
@Profile("eng")
public class EngConfiguration {
    @Bean(name = "today")
    @Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public SimpleDateFormat simpleDateFormat() {
        return new SimpleDateFormat("EEEE, MMMM, dd, yyyy", Locale.ENGLISH);
    }
}
