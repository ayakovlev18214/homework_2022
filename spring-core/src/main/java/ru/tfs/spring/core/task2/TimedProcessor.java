package ru.tfs.spring.core.task2;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class TimedProcessor implements BeanPostProcessor {
    private final MetricStatProvider metricStatProvider;
    private Set<Method> annotatedMethods;

    public TimedProcessor(MetricStatProvider metricStatProvider) {
        this.metricStatProvider = metricStatProvider;
        this.annotatedMethods = new HashSet<>();
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (!annotatedMethods.isEmpty()) {
            Object proxy = Proxy.newProxyInstance(
                    bean.getClass().getClassLoader(),
                    bean.getClass().getInterfaces(),
                    new TimedProxy(metricStatProvider, bean, annotatedMethods)
            );
            annotatedMethods.clear();
            return proxy;
        }
        return bean;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean.getClass().isAnnotationPresent(Timed.class)) {
            annotatedMethods = Set.of(bean.getClass().getMethods());
        } else {
            annotatedMethods = Arrays.stream(bean.getClass().getDeclaredMethods())
                    .filter(method -> method.isAnnotationPresent(Timed.class))
                    .collect(Collectors.toSet());
        }
        return bean;
    }
}
