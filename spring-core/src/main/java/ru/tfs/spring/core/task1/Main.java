package ru.tfs.spring.core.task1;

import java.util.Scanner;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MainConfiguration.class);
        Scanner scanner = new Scanner(System.in);
        TimeProvider timeProvider = context.getBean(TimeProvider.class);
        while (true) {
            System.out.println(timeProvider.getTime(scanner.nextLine()));
        }
    }
}
