package ru.tfs.spring.core.task1;

import java.text.SimpleDateFormat;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

@Configuration
@ComponentScan("ru.tfs.spring.core.task1")
public class MainConfiguration {
    @Bean(name = "today-iso")
    @Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public SimpleDateFormat simpleDateFormatIso() {
        return new SimpleDateFormat("yyyy-MM-dd");
    }
}
