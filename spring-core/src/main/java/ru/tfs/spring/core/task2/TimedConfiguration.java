package ru.tfs.spring.core.task2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TimedConfiguration {
    @Bean
    public TimedProcessor timedProcessor() {
        return new TimedProcessor(metricStatProvider());
    }

    @Bean
    public MetricStatProvider metricStatProvider() {
        return new MetricStatProviderImpl();
    }

    @Bean
    public UselessInterface testClass() {
        return new TestClass();
    }
}
