package ru.tfs.spring.core.task2;

import java.time.LocalDateTime;

public class MethodCallStat {
    private final LocalDateTime startTime;
    private final LocalDateTime endTime;
    private final long workTime;

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public long getWorkTime() {
        return workTime;
    }

    public MethodCallStat(LocalDateTime startTime, LocalDateTime endTime, long workTime) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.workTime = workTime;
    }
}
