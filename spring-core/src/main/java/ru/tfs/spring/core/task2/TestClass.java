package ru.tfs.spring.core.task2;

interface UselessInterface {
    void doJob(long time) throws InterruptedException;
    void doNotTImedJob(long time) throws InterruptedException;
}

public class TestClass implements UselessInterface {
    @Timed
    public void doJob(long time) throws InterruptedException {
        Thread.sleep(time);
    }

    public void doNotTImedJob(long time) throws InterruptedException {
        Thread.sleep(time);
    }
}
