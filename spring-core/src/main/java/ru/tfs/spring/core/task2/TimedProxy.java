package ru.tfs.spring.core.task2;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class TimedProxy implements InvocationHandler {
    private final MetricStatProvider metricStatProvider;
    private final Map<String, Method> stringMethodMap = new HashMap<>();
    private final Object target;

    public TimedProxy(MetricStatProvider metricStatProvider, Object target, Set<Method> annotatedMethods) {
        this.metricStatProvider = metricStatProvider;
        this.target = target;
        annotatedMethods.forEach(method -> stringMethodMap.put(method.getName(), method));
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (stringMethodMap.containsKey(method.getName())){
            long timeStart = System.currentTimeMillis();
            Object res = stringMethodMap.get(method.getName()).invoke(target, args);
            long executingTime = System.currentTimeMillis() - timeStart;
            metricStatProvider.addMethodCall(method.getName(), new MethodCallStat(
                    LocalDateTime.ofInstant(
                            Instant.ofEpochMilli(timeStart),
                            ZoneId.systemDefault()
                    ),
                    LocalDateTime.ofInstant(
                            Instant.ofEpochMilli(timeStart + executingTime),
                            ZoneId.systemDefault()
                    ),
                    executingTime
            ));
            return res;
        }
        return method.invoke(target, args);
    }
}
