package ru.tfs.api_gateway.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Builder
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PersonInfoDto {
    private long id;
    private String name;
    private LocalDate birthDate;
    private String telephoneNumber;
    private String documentNumber;
    private String region;
    private String registrationAddress;
}
