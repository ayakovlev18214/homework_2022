package ru.tfs.api_gateway.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VaccinationDto {
    private LocalDate vaccinationDate;
    private String patient;
    private String documentNumber;
    private String vaccineName;
    private String vaccinationPointName;
    private String vaccinationPointAddress;
}
