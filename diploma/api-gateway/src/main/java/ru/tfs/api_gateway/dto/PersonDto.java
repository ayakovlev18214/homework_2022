package ru.tfs.api_gateway.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PersonDto {
    private String name;
    private String documentNumber;
    private LocalDate birthDate;
    private String telephoneNumber;
    private String region;
    private String address;
    private String qr;
    private LocalDate lastVaccinationDate;
    private String vaccineName;
    private String vaccinationPointName;
    private String vaccinationPointAddress;
}
