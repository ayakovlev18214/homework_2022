package ru.tfs.api_gateway.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import ru.tfs.api_gateway.dto.PersonDto;
import ru.tfs.api_gateway.dto.PersonInfoDto;
import ru.tfs.api_gateway.dto.VaccinationDto;

@RestController
@RequiredArgsConstructor
public class ApiController {
    private final WebClient.Builder personClient;
    private final WebClient.Builder medicalClient;
    private final WebClient.Builder qrClient;

    @GetMapping("/api/v1/info/{passport}")
    public Mono<PersonDto> getPersonInfo(@PathVariable String passport) {
        Mono<PersonInfoDto> personMono = personClient.baseUrl("lb://person").build()
                .get()
                .uri("/person/passport/" + passport)
                .exchangeToMono(response -> {
                    if (response.statusCode().equals(HttpStatus.OK)) {
                        return response.bodyToMono(PersonInfoDto.class);
                    } else if (response.statusCode().is4xxClientError()) {
                        return Mono.empty();
                    } else {
                        return response.createException()
                                .flatMap(Mono::error);
                    }
                });
        Mono<VaccinationDto> vaccineMono = medicalClient.baseUrl("lb://medical-service").build()
                .get()
                .uri("/vaccination?document=" + passport)
                .exchangeToMono(response -> {
                    if (response.statusCode().equals(HttpStatus.OK)) {
                        return response.bodyToMono(VaccinationDto.class);
                    } else if (response.statusCode().is4xxClientError()) {
                        return Mono.empty();
                    } else {
                        return response.createException()
                                .flatMap(Mono::error);
                    }
                });
        Mono<String> qrMono = qrClient.baseUrl("lb://qr-service").build()
                .get()
                .uri("/qr/" + passport)
                .exchangeToMono(response -> {
                    if (response.statusCode().equals(HttpStatus.OK)) {
                        return response.bodyToMono(String.class);
                    } else if (response.statusCode().is4xxClientError()) {
                        return Mono.empty();
                    } else {
                        return response.createException()
                                .flatMap(Mono::error);
                    }
                });
        PersonDto.PersonDtoBuilder builder = PersonDto.builder();
        return Mono.zip(personMono, vaccineMono, qrMono).map(objects -> {
            PersonInfoDto personInfoDto = objects.getT1();
            VaccinationDto vaccinationDto = objects.getT2();
            builder.qr(objects.getT3())
                    .address(personInfoDto.getRegistrationAddress())
                    .name(personInfoDto.getName())
                    .region(personInfoDto.getRegion())
                    .documentNumber(personInfoDto.getDocumentNumber())
                    .telephoneNumber(personInfoDto.getTelephoneNumber())
                    .birthDate(personInfoDto.getBirthDate())
                    .lastVaccinationDate(vaccinationDto.getVaccinationDate())
                    .vaccinationPointName(vaccinationDto.getVaccinationPointName())
                    .vaccineName(vaccinationDto.getVaccineName())
                    .vaccinationPointAddress(vaccinationDto.getVaccinationPointAddress());
            return builder.build();
        });
    }
}
