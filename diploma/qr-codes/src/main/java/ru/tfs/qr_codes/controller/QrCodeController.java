package ru.tfs.qr_codes.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.tfs.qr_codes.service.QrCodeService;

@RestController
@RequiredArgsConstructor
public class QrCodeController {
    private final QrCodeService qrCodeService;

    @GetMapping("/qr/{passport}")
    String getQrCodeByPassport(@PathVariable String passport) {
       return qrCodeService.getQrCode(passport);
    }

    @GetMapping("/qr/check")
    Boolean validateQrCode(@RequestParam String code) {
        return qrCodeService.validateQrCode(code);
    }
}
