package ru.tfs.qr_codes.service;

public interface QrCodeService {
    String getQrCode(String passport);
    boolean validateQrCode(String qrCode);
}
