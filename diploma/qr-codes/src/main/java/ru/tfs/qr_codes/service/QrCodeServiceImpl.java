package ru.tfs.qr_codes.service;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

import lombok.RequiredArgsConstructor;
import org.apache.commons.codec.digest.Md5Crypt;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import ru.tfs.spring.cloud.dto.QrCodeInfo;
import ru.tfs.qr_codes.model.QrCode;
import ru.tfs.qr_codes.repository.QrCodeRepository;

@Service
@RequiredArgsConstructor
public class QrCodeServiceImpl implements QrCodeService {
    private static final String KAFKA_TOPIC = "qr.codes";
    private final QrCodeRepository repository;

    @Override
    public boolean validateQrCode(String qrCode) {
        return repository.findByQrCode(qrCode).isPresent();
    }

    @Override
    public String getQrCode(String passport) {
        return repository.findFirstByPersonDocumentNumberOrderByCreationTimeDesc(passport)
                .orElseThrow(() -> {
                    throw new IllegalArgumentException("Qr code for this passport doesn't exists.");
                }).getQrCode();
    }

    @KafkaListener(topics = KAFKA_TOPIC)
    public void processNewQrCode(QrCodeInfo qrCodeInfo) {
        if (repository.findByPersonDocumentNumberAndVaccinationDateAndPersonName(
                qrCodeInfo.getPersonDocumentNumber(),
                qrCodeInfo.getVaccinationDate(),
                qrCodeInfo.getPersonName()
        ).isEmpty()) {
            QrCode qrCode = new QrCode();
            qrCode.setQrCode(Md5Crypt.md5Crypt(
                    (Md5Crypt.md5Crypt(qrCodeInfo.getPersonName().getBytes(StandardCharsets.UTF_8))
                            + Md5Crypt.md5Crypt(qrCodeInfo.getPersonDocumentNumber().getBytes(StandardCharsets.UTF_8))
                    ).getBytes(StandardCharsets.UTF_8)
            ));
            qrCode.setPersonName(qrCodeInfo.getPersonName());
            qrCode.setPersonDocumentNumber(qrCodeInfo.getPersonDocumentNumber());
            qrCode.setCreationTime(LocalDate.now());
            qrCode.setVaccinationDate(qrCodeInfo.getVaccinationDate());
            repository.save(qrCode);
        }
    }

}
