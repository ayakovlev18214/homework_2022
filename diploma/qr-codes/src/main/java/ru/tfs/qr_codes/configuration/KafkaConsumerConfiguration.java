package ru.tfs.qr_codes.configuration;

import java.util.HashMap;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import ru.tfs.spring.cloud.dto.QrCodeInfo;

@Configuration
public class KafkaConsumerConfiguration {
    @Bean
    public ConsumerFactory<String, QrCodeInfo> consumerFactory() {
        var configProps = new HashMap<String, Object>();
        configProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:29092");
        configProps.put(ConsumerConfig.GROUP_ID_CONFIG, "qr.codes");
        configProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        var deserializer = new JsonDeserializer<QrCodeInfo>();
        deserializer.addTrustedPackages("ru.tfs.spring.cloud.dto");
        DefaultKafkaConsumerFactory<String, QrCodeInfo> factory = new DefaultKafkaConsumerFactory<>(configProps);
        factory.setValueDeserializer(deserializer);
        return factory;
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, QrCodeInfo> kafkaListenerContainerFactory() {
        var factory = new ConcurrentKafkaListenerContainerFactory<String, QrCodeInfo>();
        factory.setConsumerFactory(consumerFactory());
        factory.setConcurrency(1);
        return factory;
    }
}
