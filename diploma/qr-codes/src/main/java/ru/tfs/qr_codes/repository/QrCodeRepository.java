package ru.tfs.qr_codes.repository;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import ru.tfs.qr_codes.model.QrCode;

public interface QrCodeRepository extends CrudRepository<QrCode, Long> {
    Optional<QrCode> findFirstByPersonDocumentNumberOrderByCreationTimeDesc(String personDocumentNumber);
    Optional<QrCode> findByQrCode(String qrCode);
    Optional<QrCode> findByPersonDocumentNumberAndVaccinationDateAndPersonName(String personDocumentNumber, LocalDate vaccinationDate, String personName);
}
