package ru.tfs.qr_codes.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class QrCode {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue
    private Long id;
    private String personName;
    private String personDocumentNumber;
    private String qrCode;
    private LocalDate creationTime;
    private LocalDate vaccinationDate;
}
