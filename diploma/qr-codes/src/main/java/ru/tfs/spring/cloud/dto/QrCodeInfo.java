package ru.tfs.spring.cloud.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
public class QrCodeInfo {
    private String personName;
    private String personDocumentNumber;
    private LocalDate vaccinationDate;
}
